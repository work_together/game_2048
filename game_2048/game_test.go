// test_2048
package main

import (
	"testing"
)

func TestBuildGrid(t *testing.T) {
	grid := Grid{Size: 6, StartCells: 3}
	grid.Build()

	//if grid.Size*grid.Size-len(grid.EmptyCells()) != 5 {
	if grid.Size*grid.Size-len(grid.EmptyCells()) != grid.StartCells {
		t.Error("Fail")
	}
}

func TestNewTile(t *testing.T) {
	grid := Grid{Size: 4, StartCells: 6}
	grid.Build()

	grid.MoveLeft()
	prev := len(grid.EmptyCells())

	//	grid.newTile()
	grid.newTile()
	next := len(grid.EmptyCells())

	if prev-next == 0 {
		t.Error("NewTile not created!")
	} else if prev-next > 1 {
		t.Error("Created more than one new Tile!")
	}
}
